import './App.css';
import React, {useState} from 'react'
import axios from 'axios';

function App() {
  const [age, setAge] = useState("0")
  const [name, setName] = useState("");

  const predictAge = () =>{
    if(name !== '')
    axios.get(`https://api.agify.io/?name=${name}`).then(response => {
      let data = response.data
      setAge(data.age)
    })
  }

  return (
    <div className="container App">
      <center>
      <div className="card card-border">
        <div className="card">
          <div className="card-body">
            <div className="form-group name-label">
              <h6>Your Name</h6>
              <input type="text" className="form-control" value={name} onChange={e => setName(e.target.value)}></input>
            </div>
            <button className="btn btn-primary btn-cal" onClick={predictAge}>Calculate</button>
          </div>
        </div>
        <h5 className="card-title">Your age is {age}</h5>
      </div>
    </center>
    </div>
  );
}
export default App;
